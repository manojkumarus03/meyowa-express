const express = require('express');
const cors = require('cors');
const fs = require('fs');
const papa = require("papaparse");

const PORT = 8080;
const app = express();

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.send('Server is running...');
});

app.post('/user', (req, res) => {
    const user = req.body;
    console.log(user);

    const folderName = user.userEmail;
    const fileName = user.userEmail;
    const filePath = `./${folderName}/${fileName}.csv`;
    const csvData = papa.unparse([user]);

    if (!fs.existsSync(`./${folderName}`)) {
        fs.mkdirSync(`./${folderName}`);
    }
    fs.writeFileSync(filePath, csvData, 'utf-8');
    res.send(user);
});

app.listen(PORT, () => console.log(`Server started on http://localhost:${PORT}`));